php-codesniffer (3.11.2-2) unstable; urgency=medium

  * (debci) Exclude class for phpab
  * Skip tests currently failing on debci

 -- David Prévot <taffit@debian.org>  Mon, 23 Dec 2024 15:19:21 +0100

php-codesniffer (3.11.2-1) unstable; urgency=medium

  [ jrfnl ]
  * Generators HTML/Markdown: don't print header/footer when there are no docs
  * Generators/Markdown: reset error_reporting to original value
  * Generators/HTML: only display a TOC when there is more than one doc
  * Generators/Markdown: fix footer not parsing
  * Generators/HTML: fix line break handling
  * Generic/OpeningFunctionBraceKernighanRitchie: improve error message
  * Generators/Markdown: fix line break and indentation handling
  * Generators HTML/Markdown: fix whitespace handling in code title
  * Changelog for the 3.11.2 release

  [ Rodrigo Primo ]
  * Generic/UpperCaseConstantName: handle unconventional spacing and comments after `define(`
  * Generic/UpperCaseConstantName: handle unconventional spacing and comments before `define`
  * Generic/UpperCaseConstantName: false positives when constant name is "DEFINE"
  * Generic/UpperCaseConstantName: fix attributes false positive
  * Generic/UpperCaseConstantName: fix false positive for class instantiation
  * Squiz/ArrayDeclaration: improve handling of short lists inside a foreach

  [ David Prévot ]
  * Use php-bcmath for tests

 -- David Prévot <taffit@debian.org>  Thu, 19 Dec 2024 19:15:09 +0100

php-codesniffer (3.11.1-1) unstable; urgency=medium

  [ jrfnl ]
  * Docs: fix up two XML docs
  * Common::getSniffCode(): be more lenient about sniffs not following
    naming conventions
  * Generators: add initial set of tests (#671)
  * Changelog for the 3.11.1 release

  [ Bartosz Dziewoński ]
  * Tests: Add unit tests for Common::prepareForOutput()

  [ David Prévot ]
  * Actually exclude nophpunit11 tests

 -- David Prévot <taffit@debian.org>  Wed, 20 Nov 2024 14:07:21 +0100

php-codesniffer (3.11.0-1) unstable; urgency=medium

  [ jrfnl ]
  * :sparkles: New `Generic.WhiteSpace.HereNowdocIdentifierSpacing` sniff
  * :sparkles: New `Generic.Strings.UnnecessaryHeredoc` sniff (#633)
  * Squiz/Heredoc: add XML doc
  * PHP 8.3 | Tokenizer/PHP: support "yield from" with comments
  * Changelog for the 3.11.0 release

  [ Rodrigo Primo ]
  * Generic/ArrayIndent: add XML documentation (#432)

 -- David Prévot <taffit@debian.org>  Fri, 15 Nov 2024 14:20:34 +0100

php-codesniffer (3.10.3-2) unstable; urgency=medium

  * Adapt tests to PHPUnit 11 (Closes: #1070522)

 -- David Prévot <taffit@debian.org>  Sat, 12 Oct 2024 16:24:13 +0100

php-codesniffer (3.10.3-1) unstable; urgency=medium

  [ jrfnl ]
  * AbstractPatternSniff: prevent PHP notice
  * Squiz/OperatorBracket: prevent PHP notices during live coding
  * Tokenizer: apply tab replacement to heredoc/nowdoc opener
  * Squiz/SelfMemberReference: bug fix - false negative with namespace keyword
    as operator
  * Squiz/MultiLineFunctionDeclaration: bug fix - skip over attributes
  * Squiz/DisallowMultipleAssignments: bug fix - dynamic property assignment on
    object stored in array
  * Changelog for the 3.10.3 release

  [ Rodrigo Primo ]
  * Squiz/EmbeddedPhp: fix false positive when handling short open tags

  [ Marek ]
  * Generic.PHP.LowerCaseKeyword: require lowercase anonymous class keyword

  [ Dan Wallis ]
  * Fix conflict within PSR12.ControlStructures.ControlStructureSpacing

 -- David Prévot <taffit@debian.org>  Sat, 21 Sep 2024 10:40:05 +0200

php-codesniffer (3.10.2-1) unstable; urgency=medium

  [ jrfnl ]
  * Changelog for the 3.10.2 release

  [ David Prévot ]
  * Update Standards-Version to 4.7.0
  * Update copyright

 -- David Prévot <taffit@debian.org>  Tue, 23 Jul 2024 22:16:35 +0900

php-codesniffer (3.9.2-1) unstable; urgency=medium

  [ jrfnl ]
  * Changelog for the 3.9.2 release

  [ David Prévot ]
  * Update repository source
  * Adjust package to composer source
  * Update copyright (years)
  * Update manpages
  * Adaptations for debci

 -- David Prévot <taffit@debian.org>  Wed, 15 May 2024 22:01:56 +0200

php-codesniffer (3.7.2-2) unstable; urgency=medium

  * Upload to unstable now that Bookworm has been released
  * Compatibility with recent PHPUnit (10), partially addresses #1039759

 -- David Prévot <taffit@debian.org>  Sat, 06 Jan 2024 08:52:08 +0100

php-codesniffer (3.7.2-1) experimental; urgency=medium

  * Upload to experimental during the freeze

  [ jrfnl ]
  * Squiz/DisallowComparisonAssignment: bug fix - ignore match structures
  * Runner: show actionable information when PHPCS runs out of memory
  * Tokenizer/PHP: bug fix for short list tokens in control structures
    without braces
  * PHP 7.3 | Generic/DisallowTabIndent: add support for flexible
    heredocs/nowdocs
  * Tokenizer: apply tab replacement to heredoc/nowdoc closers
  * Generic/LowerCaseType: examine types in arrow function declarations
  * PHP 8.1 | Generic/SpreadOperatorSpacingAfter: ignore spread operator in
    first class callables

  [ Greg Sherwood ]
  * Squiz.Formatting.OperatorBracket no longer reports false positives in
    match() structures
  * Fixed bug #3666 : PEAR.Functions.FunctionCallSignature incorrect indent
    fix when checking mixed HTML/PHP files
  * Fixed bug #3672 : Incorrect ScopeIndent.IncorrectExact report for match
    inside array literal
  * Prepare for 3.7.2 release

  [ enl ]
  * Handle child process failing

  [ Jaroslav Hanslík ]
  * Fixed false positives for match() in OperatorSpacingSniff

  [ Gary Jones ]
  * Add "static analysis" Composer keyword

 -- David Prévot <taffit@debian.org>  Thu, 23 Feb 2023 06:47:10 +0100

php-codesniffer (3.7.1-2) unstable; urgency=medium

  * Drop test data set failing as-is with PHP 8.2 (Closes: #1023381)
  * Update standards version to 4.6.2, no changes needed.

 -- David Prévot <taffit@debian.org>  Tue, 10 Jan 2023 11:53:13 +0100

php-codesniffer (3.7.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.7.1 release

  [ David Prévot ]
  * Update Standards-Version to 4.6.1
  * Update expected output for tests

 -- David Prévot <taffit@debian.org>  Sat, 18 Jun 2022 16:57:53 +0200

php-codesniffer (3.6.2-1) unstable; urgency=medium

  [ Andy Postnikov ]
  * Fix deprecation for PHP 8.1

  [ Greg Sherwood ]
  * Prepare for 3.6.2 release

  [ David Prévot ]
  * Install dh-sequence-* instead of using dh --with

 -- David Prévot <taffit@debian.org>  Mon, 13 Dec 2021 11:28:26 -0400

php-codesniffer (3.6.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.6.1 release

  [ Scott Dutton ]
  * Normalise License file

  [ David Prévot ]
  * Update copyright
  * Fix test

 -- David Prévot <taffit@debian.org>  Thu, 14 Oct 2021 08:07:45 -0400

php-codesniffer (3.6.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.6.0 release

  [ David Prévot ]
  * Simplify “gbp import-orig --uscan” workflow
  * Update copyright (years)
  * Compatibility with recent PHPUnit (9)
  * Add more token-based index exclusions for the unit tests
  * Update standards version to 4.6.0, no changes needed

 -- David Prévot <taffit@debian.org>  Sat, 09 Oct 2021 16:14:58 -0400

php-codesniffer (3.5.8-3) unstable; urgency=medium

  * Drop test requiring an update every year (Closes: #980659)

 -- David Prévot <taffit@debian.org>  Wed, 20 Jan 2021 18:24:56 -0400

php-codesniffer (3.5.8-2) unstable; urgency=medium

  * Adapt to recent version of PHPUnit (9)

 -- David Prévot <taffit@debian.org>  Sun, 13 Dec 2020 10:30:21 -0400

php-codesniffer (3.5.8-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.5.8 release

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1
  * Extend workaround for CI

 -- David Prévot <taffit@debian.org>  Sun, 22 Nov 2020 15:31:26 -0400

php-codesniffer (3.5.6-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.5.6 release

  [ David Prévot ]
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Tue, 18 Aug 2020 12:19:37 -0400

php-codesniffer (3.5.5-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.5.5 release

  [ David Prévot ]
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test

 -- David Prévot <taffit@debian.org>  Sat, 30 May 2020 14:24:17 -1000

php-codesniffer (3.5.4-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.5.4 release

  [ David Prévot ]
  * Update copyright (years)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.
  * Drop temporary workaround for php-{,php}seclib related migration
  * Update Standards-Version to 4.5.0

 -- David Prévot <taffit@debian.org>  Sun, 02 Feb 2020 09:23:09 -1000

php-codesniffer (3.5.3-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.5.3 release

  [ David Prévot ]
  * Set upstream metadata fields: Repository-Browse.

 -- David Prévot <taffit@debian.org>  Fri, 06 Dec 2019 16:33:07 -1000

php-codesniffer (3.5.2-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Fixed bug #2654:
    Incorrect indentation for arguments of multiline function calls
  * Prepare for 3.5.2 release

 -- David Prévot <taffit@debian.org>  Sun, 27 Oct 2019 20:19:23 -1000

php-codesniffer (3.5.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.5.1 release

 -- David Prévot <taffit@debian.org>  Fri, 18 Oct 2019 18:09:44 -1000

php-codesniffer (3.5.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.5.0 release

  [ David Prévot ]
  * Update manpages
  * Always load FileList.php for testsuite
  * Set upstream metadata fields: Repository.
  * Update explicit Breaks against old phpseclib
  * Fix testsuite call
  * Drop versioned dependency satisfied in (old)stable
  * Update Standards-Version to 4.4.1

 -- David Prévot <taffit@debian.org>  Fri, 27 Sep 2019 09:10:21 -1000

php-codesniffer (3.4.2-3) unstable; urgency=medium

  * Add explicit Breaks against old php-phpseclib

 -- David Prévot <taffit@debian.org>  Mon, 02 Sep 2019 10:34:49 -1000

php-codesniffer (3.4.2-2) unstable; urgency=medium

  * Upload to unstable now that buster has been released
  * Update standards version, no changes needed.
  * Set upstream metadata fields: Contact, Name.
  * Update Homepage URI
  * Compatibility with recent PHPUnit (8)

 -- David Prévot <taffit@debian.org>  Sat, 10 Aug 2019 18:24:43 -1000

php-codesniffer (3.4.2-1) experimental; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.4.2 release

 -- David Prévot <taffit@debian.org>  Mon, 15 Apr 2019 05:41:06 +0900

php-codesniffer (3.4.1-1) experimental; urgency=medium

  * Upload to experimental during the freeze

  [ Greg Sherwood ]
  * Prepare for 3.4.1 release

  [ David Prévot ]
  * Use debhelper-compat 12
  * Update manpages
  * Update copyright (years)
  * Mark recent tests as such

 -- David Prévot <taffit@debian.org>  Sat, 23 Mar 2019 13:30:51 -1000

php-codesniffer (3.4.0-2) unstable; urgency=medium

  * Use allow-stderr for tests

 -- David Prévot <taffit@debian.org>  Tue, 25 Dec 2018 06:50:33 +1030

php-codesniffer (3.4.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.4.0 release

  [ David Prévot ]
  * Update copyright (years)
  * Update Standards-Version to 4.3.0

 -- David Prévot <taffit@debian.org>  Mon, 24 Dec 2018 12:24:54 +1030

php-codesniffer (3.3.2-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.3.2 release

  [ David Prévot ]
  * Use debhelper-compat 11
  * Get rid of get-orig-source target
  * Use https in Format
  * Use https in d/watch
  * Update Standards-Version to 4.2.1

 -- David Prévot <taffit@debian.org>  Mon, 24 Sep 2018 20:47:13 -1000

php-codesniffer (3.3.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.3.1 release

  [ David Prévot ]
  * Update Standards-Version to 4.1.5

 -- David Prévot <taffit@debian.org>  Sat, 28 Jul 2018 16:19:27 +0800

php-codesniffer (3.3.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.3.0 release

  [ David Prévot ]
  * Update Standards-Version to 4.1.4

 -- David Prévot <taffit@debian.org>  Wed, 20 Jun 2018 09:21:19 -1000

php-codesniffer (3.2.3-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.2.3 release

  [ David Prévot ]
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.3
  * Update manpages

 -- David Prévot <taffit@debian.org>  Tue, 27 Feb 2018 13:46:52 -1000

php-codesniffer (3.2.2-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.2.2 release

 -- David Prévot <taffit@debian.org>  Sun, 24 Dec 2017 09:30:48 +0530

php-codesniffer (3.2.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.2.0 release

  [ David Prévot ]
  * Update Standards-Version to 4.1.2

 -- David Prévot <taffit@debian.org>  Wed, 13 Dec 2017 12:34:45 -1000

php-codesniffer (3.1.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.1.1 release

  [ David Prévot ]
  * Update Standards-Version to 4.1.1

 -- David Prévot <taffit@debian.org>  Tue, 17 Oct 2017 15:38:44 -1000

php-codesniffer (3.1.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.1.0 release

  [ David Prévot ]
  * Update manpages
  * Update phpunit test calls
  * Update Standards-Version to 4.1.0
  * Update copyright

 -- David Prévot <taffit@debian.org>  Fri, 22 Sep 2017 19:40:38 -1000

php-codesniffer (3.0.2-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 3.0.2 release

  [ David Prévot ]
  * Update copyright (years)
  * Update manpages
  * Update Standards-Version to 4.0.1

 -- David Prévot <taffit@debian.org>  Tue, 08 Aug 2017 18:25:12 -0400

php-codesniffer (2.7.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.7.1 release

 -- David Prévot <taffit@debian.org>  Wed, 25 Jan 2017 16:23:54 -1000

php-codesniffer (2.7.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.7.0 release

  [ David Prévot ]
  * Update manpages

 -- David Prévot <taffit@debian.org>  Thu, 22 Sep 2016 16:37:41 -1000

php-codesniffer (2.6.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.6.1 release

 -- David Prévot <taffit@debian.org>  Thu, 02 Jun 2016 15:12:00 -0400

php-codesniffer (2.6.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.6.0 release

  [ David Prévot ]
  * Update manpages
  * Update Standards-Version to 3.9.8

 -- David Prévot <taffit@debian.org>  Wed, 06 Apr 2016 22:03:45 -0400

php-codesniffer (2.5.1-2) unstable; urgency=medium

  * Document needed extensions in package.xml (Closes: #816248)
  * Rebuild with latest pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Sat, 05 Mar 2016 18:14:50 -0400

php-codesniffer (2.5.1-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.5.1 release

  [ David Prévot ]
  * Update Standards-Version to 3.9.7

 -- David Prévot <taffit@debian.org>  Wed, 10 Feb 2016 20:07:05 -0400

php-codesniffer (2.5.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.5.0 release

  [ John Maguire ]
  * Add bootstrap file option

  [ David Prévot ]
  * Update manpages

 -- David Prévot <taffit@debian.org>  Sun, 13 Dec 2015 11:18:33 -0400

php-codesniffer (2.4.0-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.4.0 release

  [ David Prévot ]
  * Update phpcs.1
  * Use installed class for DEP-8 tests
  * Remove Thomas from uploaders following his request at last DebConf
  * Use default gbp layout

 -- David Prévot <taffit@debian.org>  Wed, 25 Nov 2015 12:04:18 -0400

php-codesniffer (2.3.4-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.3.4 release

 -- David Prévot <taffit@debian.org>  Sat, 12 Sep 2015 15:02:30 -0400

php-codesniffer (2.3.3-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.3.3 release

 -- David Prévot <taffit@debian.org>  Wed, 24 Jun 2015 13:41:36 -0400

php-codesniffer (2.3.2-1) unstable; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.3.2 release

 -- David Prévot <taffit@debian.org>  Thu, 30 Apr 2015 18:14:08 -0400

php-codesniffer (2.3.1-1) unstable; urgency=medium

  * Upload to unstable since Jessie is being released

  [ Greg Sherwood ]
  * Prepare for 2.3.1 release

 -- David Prévot <taffit@debian.org>  Sat, 25 Apr 2015 11:41:28 -0400

php-codesniffer (2.3.0-1) experimental; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.3.0 release

 -- David Prévot <taffit@debian.org>  Mon, 09 Mar 2015 19:12:55 -0400

php-codesniffer (2.2.0-1) experimental; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.2.0 release

  [ David Prévot ]
  * Update manpage
  * Update copyright

 -- David Prévot <taffit@debian.org>  Thu, 29 Jan 2015 18:28:56 -0400

php-codesniffer (2.1.0-1) experimental; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.1.0 release

 -- David Prévot <taffit@debian.org>  Tue, 23 Dec 2014 10:47:18 -0400

php-codesniffer (2.0.0-1) experimental; urgency=medium

  [ Greg Sherwood ]
  * Prepare for 2.0.0 release

 -- David Prévot <taffit@debian.org>  Fri, 12 Dec 2014 09:46:52 -0400

php-codesniffer (2.0.0~rc4-1) experimental; urgency=medium

  [ Thibaud Fabre ]
  * PHP4 style constructor warning only when __construct() is not declared

  [ Greg Sherwood ]
  * Fixed bug #295 : ScopeIndentSniff hangs when processing nested closures
  * Support for phpcs-only and phpcbf-only attributes has been added to all
    ruleset.xml elements
  * File-wide sniff properties can now be set using T_INLINE_HTML content
    during unit test runs
  * Unit tests can now set command line arguments during a test run
  * Generic ScopeIndentSniff can now fix indents using tabs instead of spaces
  * Generic DisallowSpaceIndentSniff is now able to replace space indents with
    tab indents during fixing
  * Fixed bug #298 : False positive in ScopeIndentSniff when anonymous
    functions are used with method chaining
  * Improved support for single line doc blocks
  * Fixed bug #302 : Fixing code in Squiz InlineComment sniff can remove some
    comment text
  * Fixed bug #303 : Open and close tag on same line can cause a PHP notice
    checking scope indent
  * Running unit tests with the -v CLI argument no longer generates PHP errors
  * Fixed bug #307 : Conditional breaks in case statements get incorrect
    indentions
  * Fixed bug #306 : File containing only a namespace declaration raises
    undefined index notice
  * JS tokenizer now detects xor statements correctly
  * Improved detection of properties and objects in the JS tokenizer
  * Generic MultipleStatementAlignmentSniff no longer skips assignments for
    closures
  * Generic ScopeIndentSniff now checks and auto-fixes JS files
  * Fixed bug #310 : Variadics not recognized by tokenizer
  * Fixed bug #308 : Squiz InlineIfDeclarationSniff fails on ternary operators
    inside closure
  * Prepare for 2.0.0RC4 release

  [ David Prévot ]
  * Simplify DEP-8 tests
  * Drop php-timer from build-dependencies
  * Add php_path to the config file

 -- David Prévot <taffit@debian.org>  Tue, 11 Nov 2014 15:26:20 -0400

php-codesniffer (2.0.0~rc3-1) experimental; urgency=medium

  [ Greg Sherwood ]
  * Generic DisallowTabIndent and DisallowSpaceIndent sniffs now detect
    indents at the start of block comments
  * Generic DisallowTabIndent and DisallowSpaceIndent sniffs now detect
    indents inside multi-line strings
  * Generic DisallowTabIndentSniff now replaces tabs inside doc block comments
  * Improved default output for PHPCBF and removed the option to print verbose
    output
  * Squiz ControlStructureSpacingSniff error codes have been corrected; they
    were reversed
  * Squiz EmbeddedPhpSniff now checks open and close tag indents and fixes
    some errors
  * Generic ScopeIndentSniff has been completely rewritten to improve fixing
    and embedded PHP detection
  * If a .fixed file is supplied for a unit test file, the auto fixes will be
    checked against it during testing
  * Fixer token replacement methods now return TRUE if the change was accepted
    and FALSE if rejected
  * PHPCBF already shows progress, so remove the flag
  * Setting and removing config values now prints a message to confirm the
    action and show old values
  * Squiz FileCommentSniff no longer throws incorrect blank line before
    comment errors in JS files
  * Fixed bug #280 : The --config-show option generates error when there is no
    config file
  * Squiz ClassDeclarationSniff now has better checking for blank lines after
    a closing brace
  * Removed error Squiz.Classes.ClassDeclaration.NoNewlineAfterCloseBrace
    (request #285)
  * Prepare for 2.0.0RC3 release
  * Fixer now uses temporary system files for diffing rather than creating them
    in the current working dir

  [ Ken Guest ]
  * Some enhancements re config data on command line.

 -- David Prévot <taffit@debian.org>  Mon, 20 Oct 2014 21:16:34 -0400

php-codesniffer (2.0.0~rc2-1) experimental; urgency=medium

  [ Greg Sherwood ]
  * Improved closure support in Generic ScopeIndentSniff
  * Improved indented PHP tag support in Generic ScopeIndentSniff
  * Standards can now be located within hidden directories
    (further fix for bug #20323)
  * Fixed incorrect length of JS object operator tokens
  * Exclude patterns now use backticks and work the same for files and sniffs
  * Squiz ConcatenationSpacingSniff now has a setting to specify how many
    spaces there should around concat operators
  * Fixed bug #20373 : Inline comment sniff tab handling way
  * Added fixes for the Squiz InlineCommentSniff
  * Fixed bug #20378 : Report appended to existing file if no errors
    found in run
  * Fixed bug #20377 : Error when trying to execute phpcs with report=json
  * Generic DocCommentSniff now correctly fixes additional blank lines at
    the end of a comment
  * Fixed bug #20381 : Invalid "Comment closer must be on a new line"
  * Squiz OperatorBracketSniff now correctly fixes operations that
    include arrays
  * Zend ClosingTagSniff fix now correctly leaves closing tags when
    followed by HTML
  * Fixed bug #243 : Missing DocBlock not detected
  * PHP tokenizer no longer converts class/function names to special
    tokens types
  * Squiz and PEAR FunctionCommentSnif now support _()
  * PEAR ValidFunctionNameSniff no longer throws an error for _()
  * Fixed bug #248 : FunctionCommentSniff expects ampersand on param name
  * Fixed bug #248 in Squiz sniff as well
  * Fixed bug #20402 : SVN pre-commit hook fails due to unknown argument error
  * Minified JS and CSS files are now detected and skipped
    (fixes bug #252 and bug #19899)
  * Generic DisallowTabIndentSniff now checks for, and fixes,
    mixed indents again
  * Improved fixing of mixed line indents in Generic ScopeIndentSniff
  * PHPCS can now exit with 0 if only warnings were found (request #262)
  * Generic ForbiddenFunctionsSniff now has a public property called
    forbiddenFunctions (request #263)
  * Prepare for 1.5.5 release

  [ Alexey ]
  * Allowed sigle undersored methods and functions

  [ Klaus Purer ]
  * Fixed sniff registration when a hidden directory appears in the path.

  [ Alexander Obuhovich ]
  * Error converted to warning lost it's $fixable flag
  * Adding sniff to verify that only one trait is defined per file

  [ Emily ]
  * Added var_dump to discouraged functions sniff

  [ Blaine Schmeisser ]
  * Add build in PHP syntax check on files.

  [ David Prévot ]
  * Revert "Add XS-Testsuite still needed for ci.d.n"
  * Add self to uploaders
  * Bump standards version to 3.9.6
  * Explicit Git branch in d/control

 -- David Prévot <taffit@debian.org>  Fri, 26 Sep 2014 13:56:12 -0400

php-codesniffer (2.0.0~rc1-1) experimental; urgency=medium

  * Team upload

  [ Greg Sherwood ]
  * PHPCBF will now fix incorrect newline characters in a file
  * Fixed bug #20261 : phpcbf has an endless fixing loop
  * PHPCBF now exists cleanly when there are no errors to fix
  * Removed use of sys_get_temp_dir() as this is not supported by the min PHP
    version
  * Fixed bug #20268 : Incorrect documentation titles in PEAR documentation
  * Generic DisallowSpaceIndent and DisallowTabIndent sniffs now check original
    indent content even when tab-width is set
  * Fixed a problem where PHPCBF could replace tabs with too many spaces when
    changing indents
  * Generic ScopeIndentSniff now accounts for different open tag indents
  * Exclude patterns now use {} delimiters, allowing the | special character to
    work correctly again
  * Forgot that short array token are detected by the tokenizer even on
    unsupported PHP versions
  * The filtering component of the --extensions argument is now ignored again
    when passing filenames
  * Squiz ControlSignatureSniff no longer uses the Abstract Pattern sniff so
    that it can fix these errors
  * Fixed bug #20296 : new array notion in function comma check fails
  * Fixed bug #20310 : PSR2 does not check for space after function name
  * Fixed bug #20309 : Use "member variable" term in sniff "processMemberVar"
    method
  * Fixed bug #20307 : PHP_CodeSniffer_Standards_AbstractVariableSniff analyze
    traits
  * Fixed bug #20308 : Squiz.ValidVariableNameSniff - wrong variable usage
  * Squiz InlineCommentSniff no longer requires a blank line after
    post-statement comments (request #20299)
  * Made uppercase sniff behave the same as lower + unit test fixes + changelog
  * Fixed bug #20322 : Display rules set to type=error even when suppressing
    warnings
  * Invalid sniff codes passed to --sniffs now show a friendly error message
    (request #20313)
  * Generic LineLengthSniff now shows a warning if the iconv module is disabled
    (request #20314)
  * Fixed bug #20323 : PHPCS tries to load sniffs from hidden directories
  * Squiz SelfMemberReferenceSniff now works correctly with namespaces
  * CSS tokenizer now identifies embedded PHP code using the new T_EMBEDDED_PHP
    token type
  * Fixed bug #20346 : Fixer endless loop with Squiz.CSS sniffs
  * Added fixes + checking for single space after keyword so fixes work properly
  * addFixableError() and addFixableWarning() now only return true if the fixer
    is enabled
  * Squiz ArrayDeclarationSniff now supports short array syntax
  * Added addErrorOnLine() and addWarningOnLine() to add a non-fixable violation
    to a line at column 1
  * Squiz FunctionCommentSniff is now more relaxed when checking namespaced
    type hints
  * Verbose option no longer errors when using a phar file with a space in the
    path

  [ Dawid Nowak ]
  * Correct multiline call detection: PSR2_FunctionCallSignatureSniff (string
    with multiple lines doesn't  necessarily mean multiple lines call)

  [ Weston Ruter ]
  * Allow installed_paths to be relative to the phpcs root directory

  [ David Prévot ]
  * Update phpcs.1 and phpcbf.1
  * Refresh patch
  * Add XS-Testsuite still needed for ci.d.n

 -- David Prévot <taffit@debian.org>  Sat, 09 Aug 2014 13:12:46 -0400

php-codesniffer (2.0.0~a2-1) experimental; urgency=medium

  * Team upload
  * Revert "Focus on stable release"
  * Adapt branches for experimental, and upload to experimental
  * Handle alpha releases
  * Add a manpage for PHPCBF
  * Update phpcs.1

 -- David Prévot <taffit@debian.org>  Mon, 21 Jul 2014 18:29:19 -0400

php-codesniffer (1.5.3-1) unstable; urgency=medium

  * Team upload
  * Focus on stable release
  * Update copyright
  * Bump standards version to 3.9.5
  * Update Homepage
  * Use ${phppear:…} instead of hardcoding them
  * Run tests in dh_auto_test
  * Update patch with gbp pq
  * Simplify configuration installation
  * Edit package.xml to move script
  * Add DEP-8 tests

 -- David Prévot <taffit@debian.org>  Mon, 21 Jul 2014 14:42:41 -0400

php-codesniffer (1.5.0~rc2-1) unstable; urgency=low

  * New upstream release.
  * Refreshed patch.
  * Standards-Version is now 3.9.4.
  * Canonical VCS URLs.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Jul 2013 15:16:25 +0000

php-codesniffer (1.3.4-1) unstable; urgency=low

  * Adopting the package (Closes: #679251).
  * Fixes path of CodeSniffer.conf in /etc with a patch (Closes: #470294).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Jul 2012 15:11:03 +0000

php-codesniffer (1.3.4-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release (Closes: #599617, #634825).
  * Swtiched debian/copyright to format 1.0 (rewrite was needed anyway, as the
  upstream license changed).
  * Switched package to pkg-php-tools and debhelper 8 sequencer.
  * Now running unit tests at build time (so depends on phpunit (>= 3.6)).
  * Section is now PHP.
  * Added missing Build-Depends-Indep: php-pear.
  * Added missing ${misc:Depends}.
  * Added Vcs fields.
  * Added homepage field.
  * Reviewed short and long description.
  * Added dependency on php-timer.
  * Standards-Version: is now 3.9.3 (lots of changes, see above...).

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 May 2012 16:37:24 +0000

php-codesniffer (1.1.0-1) unstable; urgency=low

  * New upstream release
  * Acknowledge NMU, thanks Jan

 -- Jack Bates <ms419@freezone.co.uk>  Wed, 01 Oct 2008 17:39:43 -0700

php-codesniffer (1.0.1-1.1) unstable; urgency=low

  * Non-maintainer upload
  * Depend on php-pear (Closes: #470291)
  * Remove some trailing whitespaces from debian/*

 -- Jan Wagner <waja@cyconet.org>  Fri, 21 Mar 2008 23:29:33 +0100

php-codesniffer (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #465179)

 -- Jack Bates <ms419@freezone.co.uk>  Thu, 07 Feb 2008 10:29:15 -0800
